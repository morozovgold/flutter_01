import 'package:equatable/equatable.dart';

class Catalog extends Equatable {
  final String name;
  final String imageUrl;

  const Catalog({
    required this.name,
    required this.imageUrl,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [name, imageUrl];

  static List<Catalog> categories = [
    const Catalog(
        name: 'logo-1', imageUrl: 'https://www.svgrepo.com/svg/353751/flutter'),
    const Catalog(
        name: 'logo-2', imageUrl: 'https://www.svgrepo.com/svg/353751/flutter'),
    const Catalog(
        name: 'logo-3', imageUrl: 'https://www.svgrepo.com/svg/353751/flutter'),
  ];
}
// TODO Implement this library.