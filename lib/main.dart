// import 'dart:html';
// import 'category_model.dart';

// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter_carousel_intro/slider_item_model.dart';
import 'package:flutter_carousel_intro/utils/enums.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'category_model.dart';
// Package imports:
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_carousel_intro/flutter_carousel_intro.dart';
// Package imports:

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(useMaterial3: true, primaryColor: Colors.blue),
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: MySlideShow(),

    );
  }
}

class MySlideShow extends StatelessWidget {
  const MySlideShow({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: FlutterCarouselIntro(
        animatedRotateX: true,
        animatedRotateZ: true,
        scale: true,
        autoPlay: true,
        animatedOpacity: false,
        autoPlaySlideDuration: const Duration(seconds: 2),
        autoPlaySlideDurationTransition: const Duration(milliseconds: 1100),
        primaryColor: const Color(0xff6C63FF),
        secondaryColor: Colors.grey,
        scrollDirection: Axis.horizontal,
        // indicatorAlign: IndicatorAlign.bottom,
        indicatorEffect: IndicatorEffects.jumping,
        showIndicators: false,

        slides: [
          SliderItem(
            // title: 'Title 1',
            // subtitle: const Text('Lorem Ipsum is simply dummy text'),
            widget: SvgPicture.asset("assets/svg/logo-1.svg"),
          ),
          SliderItem(
            // title: 'Title 2',
            // subtitle: const Text('Lorem Ipsum is simply dummy text'),
            widget: SvgPicture.asset("assets/svg/logo-2.svg"),
          ),
          SliderItem(
            // title: 'Title 3',
            widget: SvgPicture.asset("assets/svg/logo-3.svg"),
            // subtitle: ElevatedButton(
            //   onPressed: () {},
              // child: const Text("skip"),
            // ),
          ),
        ],
      ),
    );
  }
}