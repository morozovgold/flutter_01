// Dart imports:
// import 'dart:html';
import 'category_model.dart';

// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:carousel_slider/carousel_slider.dart';

class Carousel extends StatelessWidget {
  static const String routeName = '/carousel';

  const Carousel({super.key});

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const Carousel(),
    );
  }

  @override
  Widget build(BuildContext context) {
   return Scaffold(
       appBar: AppBar(
       title: const Text('Шапка приложения'),
    centerTitle: true,//текст по центру
    backgroundColor: Colors.indigo,
    ),
    body:
      CarouselSlider(
        options: CarouselOptions(
          aspectRatio: 1.5,
          viewportFraction: 0.9,
          enlargeCenterPage: true,
          // enableInfiniteScroll: false,
          enlargeStrategy: CenterPageEnlargeStrategy.height,
          // initialPage: 2,
          // autoPlay: true,
        ),
        items: Catalog.categories
            .map((category) => HeroCarouselCard(category: category))
            .toList(),
      ),

    );
  }
}

class HeroCarouselCard extends StatelessWidget {
  final Catalog category;

  const HeroCarouselCard({super.key, 
    required this.category,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(5.0),
      child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(5.0)),
          child: Stack(
            children: <Widget>[
              Image.network(
                  category.imageUrl, fit: BoxFit.cover, width: 1000.0),
              Positioned(
                bottom: 0.0,
                left: 0.0,
                right: 0.0,
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color.fromARGB(200, 0, 0, 0),
                        Color.fromARGB(0, 0, 0, 0)
                      ],
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                    ),
                  ),
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20.0),
                  child: Text(
                    category.name,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
      ),
    );
  }
}